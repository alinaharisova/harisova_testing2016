import org.openqa.selenium.By;


public class LoginHelper extends HelperBase {

    public LoginHelper(AppManager manager) {
        super(manager);
    }

    public void login(AccountData user) {
        manager.navigationHelper.goToLoginPage();
        fillTheField("j_username", user.login);
        fillTheField("j_password", user.password);
        manager.navigationHelper.button("button.login-button");
    }

    public void logout() {
        manager.driver.findElement(By.id("exit")).click();
    }

}
