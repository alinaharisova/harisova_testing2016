import org.testng.annotations.*;

public class MyTest extends TestBase{

    @Test
    public void searchProduct() {
        app.searchHelper.search("dior");
    }

    @Test
    public void addToCartProduct() {
        app.cartHelper.addToCart("Dior J'adore Eau de Toilette");
    }

    @Test
    public void addAddress() {
        app.navigationHelper.goToAccountPage();
        app.navigationHelper.goToAddreddesPage();
        AddressData address = new AddressData("Алина", "Харисова", "Татарстан респ.", "Казань", "ул Пушкина 32", "+7(987)777-77-77");
        app.addressHelper.addAddress(address);
    }

    @Test
    public void logout(){
        app.loginHelper.logout();
    }
}
