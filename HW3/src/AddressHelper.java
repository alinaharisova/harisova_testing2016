import org.openqa.selenium.By;

public class AddressHelper extends HelperBase {

    public AddressHelper(AppManager manager) {
        super(manager);
    }

    public void addAddress(AddressData address) {
        openNewArrressPage();
        fillAddressForm(address);
        submitAddressCreation();
    }

    public void submitAddressCreation() {
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
    }

    public void openNewArrressPage() {
        manager.navigationHelper.goToLink("Добавить новый адрес");
    }

    public void fillAddressForm(AddressData address) {
        fillTheField("address.firstName", address.firstName);
        fillTheField("address.surname", address.surname);
        fillTheSelect("address.region", address.region);
        fillTheSelect("address.cityIso", address.cityIso);
        fillTheField("address.line1", address.line1);
        fillTheField("address.phone", address.phone);
    }
}
