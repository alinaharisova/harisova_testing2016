import org.testng.annotations.*;

public class MyTest extends TestBase{

    @Test
    public void testMy() throws Exception {
        AddToCart("Dior J'adore Eau de Toilette");
        GoToAccountPage();
        GoToAddreddesPage();
        AddressData address = new AddressData("Алина", "Харисова", "Татарстан респ.", "Казань", "ул Пушкина 32", "+7(987)777-77-77");
        AddAddress(address);
        Logout();
    }
}
