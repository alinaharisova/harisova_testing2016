import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestBase {
    protected WebDriver driver;
    protected String baseUrl;
    protected boolean acceptNextAlert = true;
    protected StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://shop.rivegauche.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        GoToHomePage();
        AccountData userdata = new AccountData("alinusik.harisova@gmail.com", "secret");
        Login(userdata);
    }

    protected void GoToHomePage() {
        driver.get(baseUrl + "/store/");
    }

    protected void GoToLoginPage() {
        GoToLink("Вход/Регистрация");
    }

    protected void Button(String selector) {
        driver.findElement(By.cssSelector(selector)).click();
    }

    protected void GoToLink(String link) {
        driver.findElement(By.linkText(link)).click();
    }

    protected void FillTheSelect(String field, String value) {
        new Select(driver.findElement(By.id(field))).selectByVisibleText(value);
    }

    protected void Login(AccountData user) {
        GoToLoginPage();
        FillTheField("j_username", user.login);
        FillTheField("j_password", user.password);
        Button("button.login-button");
    }

    protected void FillTheField (String field, String value) {
        driver.findElement(By.id(field)).clear();
        driver.findElement(By.id(field)).sendKeys(value);
    }

    protected void Logout() {
        driver.findElement(By.id("exit")).click();
    }

    @AfterClass(alwaysRun = true)
    protected void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    protected void AddAddress(AddressData address) {
        GoToLink("Добавить новый адрес");
        FillTheField("address.firstName", address.firstName);
        FillTheField("address.surname", address.surname);
        FillTheSelect("address.region", address.region);
        FillTheSelect("address.cityIso", address.cityIso);
        FillTheField("address.line1", address.line1);
        FillTheField("address.phone", address.phone);
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
    }

    protected void GoToAddreddesPage() {
        driver.findElement(By.xpath("//div[2]/ul/a/li")).click();
    }

    protected void GoToAccountPage() {
        GoToLink("Личный кабинет");
    }

    protected void AddToCart(String product) {
        GoToLink(product);
        Button("button.addToCartButton.text-button");
    }

    protected boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    protected String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
