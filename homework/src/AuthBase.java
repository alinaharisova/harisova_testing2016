import org.testng.annotations.BeforeClass;


/**
 * Created by Alina on 12.04.2016.
 */
public class AuthBase extends TestBase {
    @BeforeClass(alwaysRun = true)
    protected void setUpAuth() throws Exception {
        app.loginHelper.login(new AccountData(app.settings.getLogin(), app.settings.getPassword()));
    }
}
