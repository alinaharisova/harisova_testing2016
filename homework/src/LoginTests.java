import org.junit.Assert;
import org.testng.annotations.Test;

/**
 * Created by Alina on 12.04.2016.
 */
public class LoginTests extends TestBase {
    @Test
    public void loginWithValidCreditionals() throws Exception {
        AccountData acc = new AccountData(app.settings.getLogin(), app.settings.getPassword());
        app.loginHelper.logout();
        app.loginHelper.login(acc);
        Assert.assertEquals(app.loginHelper.isLoggedIn(acc), true);
    }
}
