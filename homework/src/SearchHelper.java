
public class SearchHelper extends HelperBase {

    public SearchHelper(AppManager manager) {
        super(manager);
    }

    public void search(String search) {
        fillSearchForm(search);
        manager.navigationHelper.button("button.siteSearchSubmit");
    }

    public void fillSearchForm (String searchForm) {
        fillTheField("search", searchForm);
    }
}
