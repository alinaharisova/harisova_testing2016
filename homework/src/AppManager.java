import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AppManager {
    private String baseUrl;
    protected WebDriver driver;
    protected LoginHelper loginHelper;
    protected NavigationHelper navigationHelper;
    protected SearchHelper searchHelper;
    protected AddressHelper addressHelper;
    protected CartHelper cartHelper;
    protected StringBuffer verificationErrors = new StringBuffer();
    protected Settings settings;

    private static ThreadLocal <AppManager> app = new ThreadLocal <AppManager>();

    public AppManager() {
        driver = new FirefoxDriver();
		settings = new Settings();
        baseUrl = settings.getBaseUrl();
        loginHelper = new LoginHelper(this);
        navigationHelper = new NavigationHelper(this, baseUrl);
        searchHelper = new SearchHelper(this);
        addressHelper = new AddressHelper(this);
        cartHelper = new CartHelper(this);
    }


    @AfterClass(alwaysRun = true)
    protected void Stop() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    public static AppManager getInstance (){
        if (app == null) {
            AppManager newInstance = new AppManager();
            newInstance.navigationHelper.goToHomePage();
            app.set(newInstance);
        }
        return app.get();
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public AddressHelper getAddressHelper() {
        return addressHelper;
    }

    public void setAddressHelper(AddressHelper addressHelper) {
        this.addressHelper = addressHelper;
    }

    public NavigationHelper getNavigationHelper() {
        return navigationHelper;
    }

    public void setNavigationHelper(NavigationHelper navigationHelper) {
        this.navigationHelper = navigationHelper;
    }

    public SearchHelper getSearchHelper() {
        return searchHelper;
    }

    public void setSearchHelper(SearchHelper searchHelper) {
        this.searchHelper = searchHelper;
    }

    public LoginHelper getLoginHelper() {
        return loginHelper;
    }

    public void setLoginHelper(LoginHelper loginHelper) {
        this.loginHelper = loginHelper;
    }

    public CartHelper getCartHelper() {
        return cartHelper;
    }

    public void setCartHelper(CartHelper cartHelper) {
        this.cartHelper = cartHelper;
    }
}
