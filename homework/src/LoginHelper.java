import org.openqa.selenium.By;

public class LoginHelper extends HelperBase {

    public LoginHelper(AppManager manager) {
        super(manager);
    }

    public void login(AccountData user) {
        if (isLoggedIn()) {
            if (!isLoggedIn(user)){
                manager.loginHelper.logout();
            }
        }

        manager.navigationHelper.goToLoginPage();
        fillTheField("j_username", user.login);
        fillTheField("j_password", user.password);
        manager.navigationHelper.button("button.login-button");
    }

    public void logout() {
        if (isLoggedIn()) {
            manager.driver.findElement(By.id("exit")).click();
        }
    }

    public boolean isLoggedIn(){
        return isElementPresent(By.id("exit"));
    }

    public boolean isLoggedIn(AccountData account){
        return isLoggedIn() && (getUserName().equals(account.login));
    }

    public String getUserName() {
        manager.navigationHelper.goToAccountPage();
        String text = driver.findElements(By.tagName("tr")).get(3).findElements(By.tagName("tr")).get(2).getText();
        return text;
    }
}