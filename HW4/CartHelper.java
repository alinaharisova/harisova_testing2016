public class CartHelper extends HelperBase {

    public CartHelper(AppManager manager) {
        super(manager);
    }

    public void addToCart(String product) {
        manager.navigationHelper.goToLink(product);
        manager.navigationHelper.button("button.addToCartButton.text-button");
    }
}
