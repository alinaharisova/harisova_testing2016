import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;

public class TestBase {
    protected AppManager app;
    protected StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        app = AppManager.getInstance();
        app.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        app.navigationHelper.goToHomePage();
    }
}