import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alina on 12.04.2016.
 */
public class AuthBase extends TestBase {
    @BeforeClass(alwaysRun = true)
    protected void setUp() throws Exception {
        app.loginHelper.login(new AccountData("alinusik.harisova@gmail.com", "secret"));
    }
}
