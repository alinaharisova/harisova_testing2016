import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class LoginHelper extends HelperBase {

    public LoginHelper(AppManager manager) {
        super(manager);
    }

    public void login(AccountData user) {
        manager.navigationHelper.goToLoginPage();
        fillTheField("j_username", user.login);
        fillTheField("j_password", user.password);
        manager.navigationHelper.button("button.login-button");

        if (isLoggedIn()) {
            if (isLoggedIn(user)) {
                return;
            } else logout();
        }
    }

    public void logout() {
        if (isLoggedIn()) {
            manager.driver.findElement(By.id("exit")).click();
        }
    }

    public boolean isLoggedIn(){
        return manager.isElementPresent(By.id("exit"));
    }

    public boolean isLoggedIn(AccountData account){
        return isLoggedIn() && (getUserName() == account.login);
    }

    public String getUserName() {
        manager.navigationHelper.goToAccountPage();
        String text = driver.findElements(By.tagName("tr")).get(3).findElements(By.tagName("tr")).get(2).getText();
        return text;
    }
}