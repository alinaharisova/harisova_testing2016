import org.openqa.selenium.By;

public class NavigationHelper extends HelperBase {

    private String baseUrl;

    public NavigationHelper(AppManager manager, String baseUrl) {
        super(manager);
        this.baseUrl = baseUrl;
    }

    public void goToHomePage() {
        driver.get(baseUrl + "/store/");
    }

    public void goToLoginPage() {
        goToLink("Вход/Регистрация");
    }

    public void goToAddreddesPage() {
        driver.findElement(By.xpath("//div[2]/ul/a/li")).click();
    }

    public void goToAccountPage() {
        goToLink("Личный кабинет");
    }

    public void button(String selector) {
        driver.findElement(By.cssSelector(selector)).click();
    }

    public void goToLink(String link) {
        driver.findElement(By.linkText(link)).click();
    }


}
