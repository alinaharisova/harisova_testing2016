import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class MyTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://shop.rivegauche.ru/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testUntitled2() throws Exception {
        driver.get(baseUrl + "/store/");
        driver.findElement(By.linkText("Вход/Регистрация")).click();
        driver.findElement(By.id("j_username")).clear();
        driver.findElement(By.id("j_username")).sendKeys("alinusik.harisova@gmail.com");
        driver.findElement(By.id("j_password")).clear();
        driver.findElement(By.id("j_password")).sendKeys("secret");
        driver.findElement(By.cssSelector("button.login-button")).click();
        driver.findElement(By.linkText("Dior J'adore Eau de Toilette")).click();
        driver.findElement(By.cssSelector("button.addToCartButton.text-button")).click();
        driver.findElement(By.linkText("Личный кабинет")).click();
        driver.findElement(By.xpath("//div[2]/ul/a/li")).click();
        driver.findElement(By.linkText("Добавить новый адрес")).click();
        driver.findElement(By.id("address.firstName")).clear();
        driver.findElement(By.id("address.firstName")).sendKeys("Алина");
        driver.findElement(By.id("address.surname")).clear();
        driver.findElement(By.id("address.surname")).sendKeys("Харисова");
        new Select(driver.findElement(By.id("address.region"))).selectByVisibleText("Татарстан респ.");
        new Select(driver.findElement(By.id("address.cityIso"))).selectByVisibleText("Казань");
        driver.findElement(By.id("address.line1")).clear();
        driver.findElement(By.id("address.line1")).sendKeys("ул Пушкина 32");
        driver.findElement(By.id("address.phone")).clear();
        driver.findElement(By.id("address.phone")).sendKeys("+7(987)777-77-77");
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        driver.findElement(By.id("exit")).click();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}

