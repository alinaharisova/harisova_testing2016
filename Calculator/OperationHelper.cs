﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoTests
{
    public class OperationHelper : HelperBase
    {
        
        public OperationHelper(AppManager manager) : base(manager) { }

        public void EnterNumber(int number)
        {
            int a = Math.Abs(number);
            SplitBigNumber(a);
            if (number < 0)
            {
                EnterSign("+-");
            }
        }


        public void EnterNumber(double number)
        {   
            double modNumber = Math.Abs(number);
            string[] numberSplit = modNumber.ToString().Split(',');
            int a = Convert.ToInt32(numberSplit[0]);
            int b = Convert.ToInt32(numberSplit[1]);
            SplitBigNumber(a);
            EnterSign(",");
            SplitBigNumber(b);
            if (number < 0)
            {
                EnterSign("+-");
            }
        }

        private void SplitBigNumber(int number) 
        {            
            ArrayList numbers = new ArrayList();
            
            while (number != 0)
            {
                numbers.Add(number % 10);
                number = number / 10;                
            }
            numbers.Reverse();
            foreach (int num in numbers)
            {
                EnterNumber(num);
            }
        } 

        private void SwitchNumber (int number)
        {
            switch (number)
            {
                case 0:
                    aux.ControlClick(WINTITLE, "", "Button6");
                    break;
                case 1:
                    aux.ControlClick(WINTITLE, "", "Button5");
                    break;
                case 2:
                    aux.ControlClick(WINTITLE, "", "Button11");
                    break;
                case 3:
                    aux.ControlClick(WINTITLE, "", "Button16");
                    break;
                case 4:
                    aux.ControlClick(WINTITLE, "", "Button4");
                    break;
                case 5:
                    aux.ControlClick(WINTITLE, "", "Button10");
                    break;
                case 6:
                    aux.ControlClick(WINTITLE, "", "Button15");
                    break;
                case 7:
                    aux.ControlClick(WINTITLE, "", "Button3");
                    break;
                case 8:
                    aux.ControlClick(WINTITLE, "", "Button9");
                    break;
                case 9:
                    aux.ControlClick(WINTITLE, "", "Button14");
                    break;
                default:
                    break;
            }

        }

        public void EnterSign(string sign)
        {
            switch (sign)
            {
                case "+":
                    aux.ControlClick(WINTITLE, "", "Button23");
                    break;
                case "-":
                    aux.ControlClick(WINTITLE, "", "Button22");
                    break;
                case "*":
                    aux.ControlClick(WINTITLE, "", "Button21");
                    break;
                case "/":
                    aux.ControlClick(WINTITLE, "", "Button20");
                    break;
                case "+-":
                    aux.ControlClick(WINTITLE, "", "Button19");
                    break;
                case ",":
                    aux.ControlClick(WINTITLE, "", "Button17");
                    break;
                case "SQR":
                    aux.ControlClick(WINTITLE, "", "Button25");
                    break;
                case "1/x":
                    aux.ControlClick(WINTITLE, "", "Button27");
                    break;
                default:
                    break;
            }
        }

        public void Result()
        {
            aux.ControlClick(WINTITLE, "", "Button28");
        }

        public string GetResult()
        {
            string one = aux.ControlGetText(WINTITLE, "", "#327701");
            string two = aux.WinGetText(WINTITLE);
            return aux.WinGetText(WINTITLE).Replace("\n", "");
        }

        public string Calculate(OperationDataInt items)
        {
            int result = 0;
            switch (items.Sign)
            {
                case "+":
                    result = items.First + items.Second;
                    break;
                case "-":
                    result = items.First - items.Second;
                    break;
                case "*":
                    result = items.First * items.Second;
                    break;
                case "/":
                    result = items.First / items.Second;
                    break;
                default:
                    break;
            }
            return result.ToString();
        }

        public string Calculate(OperationDataDouble items)
        {
            double result = 0;
            switch (items.Sign)
            {
                case "+":
                    result = items.First + items.Second;
                    break;
                case "-":
                    result = items.First - items.Second;
                    break;
                case "*":
                    result = items.First * items.Second;
                    break;
                case "/":
                    result = items.First / items.Second;
                    break;
                case "SQR":
                    result = Math.Sqrt(items.First);
                    break;
                case "1/x":
                    result = 1 / items.First;
                    break;
                default:
                    break;
            }
            return result.ToString();
        }

        public string CalculateFunction(OperationDataInt items)
        {
            double result = 0;
            switch (items.Sign)
            {
                case "SQR":
                    result = Math.Sqrt(items.First);
                    break;
                case "1/x":
                    result = 1/items.First;
                    break;
                default:
                    break;
            }
            return result.ToString();
        }
    }
}
