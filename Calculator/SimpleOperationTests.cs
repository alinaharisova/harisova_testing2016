﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace AutoTests
{

    
    [TestFixture]
    public class SimpleOperationTests : TestBase
    {
           [Test]
           public void SimpleTest()
           {
               OperationDataInt items = new OperationDataInt()
               {
                   First = 3,
                   Sign = "+",
                   Second = 2
               }
               ;             

               manager.Operations.EnterNumber(items.First);
               manager.Operations.EnterSign(items.Sign);
               manager.Operations.EnterNumber(items.Second);
               manager.Operations.Result();

               string result = manager.Operations.GetResult();
               string shouldBe = manager.Operations.Calculate(items);

               NUnit.Framework.Assert.AreEqual(result, shouldBe);
           }

        [Test]
        public void DoubleNumbersTest()
        {
            OperationDataDouble items = new OperationDataDouble()
            {
                First = -3.5,
                Sign = "+",
                Second = 2.7
            }
            ;

            manager.Operations.EnterNumber(items.First);
            manager.Operations.EnterSign(items.Sign);
            manager.Operations.EnterNumber(items.Second);
            manager.Operations.Result();

            string result = manager.Operations.GetResult();
            string shouldBe = manager.Operations.Calculate(items);

            NUnit.Framework.Assert.AreEqual(result, shouldBe);
        }

        [Test]
        public void FunctionTest()
        {
            OperationDataInt items = new OperationDataInt()
            {
                First = 4,
                Sign = "SQR"
            }
            ;

            
            manager.Operations.EnterNumber(items.First);
            manager.Operations.EnterSign(items.Sign);
            manager.Operations.Result();

            string result = manager.Operations.GetResult();
            string shouldBe = manager.Operations.CalculateFunction(items);

            NUnit.Framework.Assert.AreEqual(result, shouldBe);
        }
        
        [Test]
        public void BigNumbersTest()
        {
            OperationDataInt items = new OperationDataInt()
            {
                First = 24234,
                Sign = "+",
                Second = 327657
            }
            ;


            manager.Operations.EnterNumber(items.First);
            manager.Operations.EnterSign(items.Sign);
            manager.Operations.EnterNumber(items.Second);
            manager.Operations.Result();

            string result = manager.Operations.GetResult();
            string shouldBe = manager.Operations.CalculateFunction(items);

            NUnit.Framework.Assert.AreEqual(result, shouldBe);
        }
    }
}
